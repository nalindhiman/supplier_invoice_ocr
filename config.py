ALLOWED_EXTENTIONS = set(["pdf"])
UPLOAD_FOLDER = 'static/uploads'
AWS_REGION = "eu-west-1"
AWS_ACCESS_KEY_ID = "AKIA6J73W5KEQRD7CEWZ"
AWS_SECRET_ACCESS_KEY = "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"
AWS_S3_BUCKET = "ocr-kyc-documents"
MYSQL_DATABASE_USER = 'root'
MYSQL_DATABASE_PASSWORD = 'root'
MYSQL_DATABASE_DB = 'suppliers_invoice_ocr'
MYSQL_DATABASE_HOST = 'localhost'

TABLE_HEADER_LABELS = {
    "Invoice_no": ['tr po-1', 'seri a sira no',
                   'sales order nr.:', 'sales invoice',
                   'proforma invoice no.', 'proforma inv',
                   'numero documento', 'nº factura proforma /proform invoice nº',
                   'nº factura', 'invoice#',
                   'invoice number', 'invoice no',
                   'invoice no.', 'invoice #', 'invoice',
                   'faktura', 'facture nº', 'facture',
                   'factura nº', 'factura',
                   'exportrechnung', 'doc num',
                   'detailed specifications invoice'],

    "Invoice_date": ['data', 'data documento',
                     'date', 'date of sales order:',
                     'doc. date', 'du', 'duzenleme tarihi',
                     'fecha', 'fecha/date', 'inv date',
                     'invoice date', 'rechnungsdatum:',
                     'Data sprzedaży'],

    "Article_number": ['codice', 'item', 'item code', 'sku'],

    "Item_reference_no.": ['art-nr/ean', 'code', 'codice articolo',
                           'descrizione', 'item code',
                           'item number/description', 'item ref',
                           'no', 'product code', 'product reference',
                           'ref', 'ref #', 'ref no.', 'reference',
                           'symbol', 'urun referans kodu',
                           'ref.', 'reference', 'pos. art-nr./ean'],

    "UPC": ['upc code', 'upc code#', 'upc',
            'referencia', 'product code', 'kod kreskowy',
            'ean code', 'ean/reference', 'ean', 'codice articolo',
            'code barre', 'code', 'barcode', 'article code', 'art-nr./ean'],

    "Item_description": ['descripcion', 'description',
                         'descrizione', 'item',
                         'item description', 'item name',
                         'item number/description', 'name',
                         'product description', 'skin care products',
                         'urun aciklamasi', 'designation',
                         'libelle du produit'],
    "Quantity": ['uni.', 'ship quantity', 'quantity items',
                 'quantity', "quantita'", 'quantita', 'qty.',
                 'qty in pcs', 'qty pcs', 'qty', 'qnty', 'q fac',
                 'pcs', 'miktari', 'menge', 'cart.qt)',
                 'cantidad/quantity', 'cantidad', 'qte', 'llosc'],
    "Item_rate": ['unit rate', 'unit price', 'px unitaire',
                  'prix unitaire', 'price per item', 'price eur',
                  'price / pce(eudo)', 'price (us$)', 'price without vat,eur',
                  'price', 'prezzo unitario', 'prezzo', 'preis',
                  'excl. iva', 'precio neto / net price', 'precio % dto.',
                  'fiyati', 'cena netto', 'price e', 'price price /pce',
                  'prezzo %sc.', 'cena', 'precio', 'unit'],

    "Discount": ['disc%', 'rabatt', 'remise'],

    "Tax": ['gst 7%', 'tax', 'vat', 'vat,eur'],

    "Net_price": ['nett unit price', 'prix net', 'net price'],

    "freight": ['freight'],

    "Insurance": ["add: 1% for Insurance"],

    "gst_amount": ['7% gst', 'add: gst 7%', 'gst amount in sgd'],

    "Exchange_rate_gst": ['exchange rate for gst purposes us$~sgd'],

    "Currency": ['us$', 'curency', 'usd', 'eur', 'currency:',
                 'euro', 'currency', 'hk$', '€', 'HKD',
                 'valuta', '$', 'euros', 'sar'],

    "Item_id": ['item id'],

    "other_charges": []

}
