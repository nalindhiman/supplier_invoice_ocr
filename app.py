from flask import Flask, render_template, request, flash, redirect
from werkzeug.utils import secure_filename
from config import ALLOWED_EXTENTIONS
import time
import os
from ocr import AWSTextract
from config import UPLOAD_FOLDER
from config import TABLE_HEADER_LABELS
import MySQLdb
from MySQLdb import escape_string as thwart
import json
from config import MYSQL_DATABASE_USER
from config import MYSQL_DATABASE_PASSWORD
from config import MYSQL_DATABASE_DB
from config import MYSQL_DATABASE_HOST
from datetime import datetime


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENTIONS


def connect_mysql():
    db = MySQLdb.connect(host=MYSQL_DATABASE_HOST,
                         user=MYSQL_DATABASE_USER,
                         passwd=MYSQL_DATABASE_PASSWORD,
                         db=MYSQL_DATABASE_DB)
    curr = db.cursor()
    return db, curr


def convert_text_to_lines(words):
    lines = {}
    for page, data in words.items():
        # sort words by top
        sort_words = sorted(data, key=lambda x: x[0][1])
        lines[page] = []
        line = []
        top = sort_words[0][0][1]
        line_height_diff = 9
        for word in sort_words:
            if abs(word[0][1] - top) >= line_height_diff:
                sorted_line = sorted(line, key=lambda x: x[0][0])
                lines[page].append(sorted_line)
                line = [word]
                top = word[0][1]
            else:
                line.append(word)
    return lines


def match_header_coordinates(updated_header_line, header_coords):
    for item_label, item_cords in header_coords.items():
        for cords in updated_header_line:
            if cords[1] in item_cords[1]:
                header_coords[item_label] = cords
                print("header_coords---------->", header_coords)
    return header_coords


# def match_header_coordinates(updated_header_line, header_coords):
#     for item_label, item_cords in header_coords.items():
#         for cords in updated_header_line:
#             if cords[1] in item_cords[1]:
#                 header_coords[item_label] = cords
#     return header_coords


def adjust_table_header(line, header_coords, width):
    sorted_line = sorted(line, key=lambda x: x[0][0])
    updated_header_line = []
    for i, word in enumerate(sorted_line):
        if i < len(sorted_line) - 1:
            if i == 0:
                updated_header_line.append(((0, word[0][1],
                                             sorted_line[i + 1][0][0] - 6,
                                             word[0][3]), word[1]))
                continue
            new_right = sorted_line[i + 1][0][0] - 3
            updated_header_line.append(((word[0][0] - 4, word[0][1],
                                         new_right, word[0][3]), word[1]))
        else:
            updated_header_line.append(((word[0][0] - 3, word[0][1],
                                         width, word[0][3]), word[1]))
    header_coords = match_header_coordinates(updated_header_line,
                                             header_coords)
    return header_coords


# def adjust_table_header(line, header_coords, width):
#     sorted_line = sorted(line, key=lambda x: x[0][0])
#     updated_header_line = []
#     ist_left = 0
#     for i, word in enumerate(sorted_line):
#         if i < len(sorted_line) - 1:
#             if i == 0:
#                 updated_header_line.append(((ist_left, word[0][1],
#                                              sorted_line[i + 1][0][0] - 6,
#                                              word[0][3]), word[1]))
#                 continue
#             words_diff = abs(word[0][2] - sorted_line[i + 1][0][0])
#             if words_diff < 15:
#                 new_right = sorted_line[i + 1][0][0] - 3
#                 new_left = word[0][0] - 2
#                 updated_header_line.append(((new_left, word[0][1],
#                                              new_right, word[0][3]), word[1]))
#             elif words_diff > 15:
#                 new_right = sorted_line[i + 1][0][0] - 5
#                 new_left = word[0][0] - 4
#                 updated_header_line.append(((new_left, word[0][1],
#                                              new_right, word[0][3]), word[1]))
#             # elif words_diff > 30:
#             #     new_right = sorted_line[i + 1][0][0] - 5
#             #     new_left = word[0][0] - 4
#             #     updated_header_line.append(((new_left, word[0][1],
#             #                                  new_right, word[0][3]), word[1]))
#             else:
#                 updated_header_line.append(((word[0][0] - 5, word[0][1],
#                                              width, word[0][3]), word[1]))
#     header_dict = match_header_coordinates(updated_header_line,
#                                            header_coords)
#     print("HD", header_dict)
#     return header_dict


def get_table_labels(lines_dict):
    for key, lines in lines_dict.items():
        for line in lines:
            table_headers = {}
            for keys, lists in TABLE_HEADER_LABELS.items():
                for word in line:
                    if word[1].lower() in lists:
                        table_headers[keys] = word
                        # import pdb; pdb.set_trace()
            if len(table_headers) >= 3:
                # print("TH", table_headers)
                return table_headers, line
        return None


def check_next_product_line(line, items, header_coords, index, lines):
    next_line = lines[index + 1]
    if len(next_line) > 3:
        return items
    else:
        for i in range(1, 3):
            if len(lines) > index + 2:
                next_line = lines[index + i]
                if len(next_line) < 3 and abs(line[0][0][1] - next_line[0][0][1]) < 13:
                    for words in next_line:
                        print("index", index + i, "  line----", next_line)
                        for cords in list(header_coords.values()):
                            words_center = ((words[0][0] + words[0][2]) // 2) + 5
                            coords_left = cords[0][0]
                            coords_right = cords[0][2]
                            if words_center > coords_left and words_center < coords_right and not words[1] in ['$', '€']:
                                items[cords[1]] += words[1]
                    line = next_line
    return items


# def get_mapped_data(lines_dict, table_top, header_coords):
#     items_list = []
#     coords_left = 0
#     coords_right = 0
#     line_top = table_top + 10
#     for key, lines in lines_dict.items():
#         for index, line in enumerate(lines):
#             if line[0][0][1] > table_top and abs(line[0][0][1] - line_top) < 40 and len(line) > 3:
#                 items = {element[1]: '' for element in list(header_coords.values())}
#                 for word in line:
#                     for cords in list(header_coords.values()):
#                         words_center = ((word[0][0] + word[0][2]) // 2) + 5
#                         coords_left = cords[0][0]
#                         coords_right = cords[0][2]
#                         if words_center > coords_left and words_center < coords_right and not word[1] in ['$', '€']:
#                             items[cords[1]] = word[1]
#                 items = check_next_product_line(line, items,
#                                                 header_coords, index, lines)
#                 items_list.append(items)
#                 line_top = line[0][0][1]
#     return items_list


def get_mapped_data(lines_dict, table_top, header_coords):
    items_list = []
    line_top = table_top + 10
    for key, lines in lines_dict.items():
        for index, line in enumerate(lines):
            if line[0][0][1] > table_top and abs(line[0][0][1] - line_top) < 40 and len(line) > 3:
                items = {element: '' for element in list(header_coords.keys())}
                for word in line:
                    for keys, cords in header_coords.items():
                        # print("line", line)
                        if (word[0][0] >= cords[0][0] and word[0][2] <= cords[0][2]):
                            items[keys] = word[1]
                items = check_next_product_line(line, items,
                                                header_coords, index, lines)
                items_list.append(items)
                line_top = line[0][0][1]
    return items_list


def get_other_invoice_details(lines_dict, table_top):
    other_details = {}
    for keys, lines in lines_dict.items():
        for line in lines:
            if line[0][0][1] < table_top and len(line) <= 3:
                for key, lists in TABLE_HEADER_LABELS.items():
                    for index, word in enumerate(line):
                        if word[1].lower() in lists and index < len(line) - 1:
                            other_details[key] = line[index + 1][1]
    return other_details


@app.route('/', methods=['GET', 'POST'])
def upload_file():

    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file uploaded to process')
            return redirect(request.url)
        # file uploaded is received here by the ui
        file = request.files['file']
        if file.filename == '':
            flash('No file uploaded to process')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            filename = str(time.time()) + "_" + secure_filename(file.filename)
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(file_path)
        # upload file to S3
        # aws_textract = AWSTextract()
        # s3_client = aws_textract.connect_S3()
        # aws_textract.upload_to_s3(s3_client,
        #                           file_path,
        #                           filename)

        # textract_client = aws_textract.connect_textract()
        # job_id = aws_textract.start_textract_job(textract_client,
        #                                          filename)
        # words, width, height = aws_textract.get_textract_job_result(
        #     textract_client, job_id, file_path, 'LINE')
        # print(words, width, height)
        words, width, height = {1: [((166, 15, 299, 24), 'LICORES DE VENEZUELA, S.A.'), ((375, 14, 532, 25), 'PROFORMA INVOICE No.'), ((561, 14, 590, 25), '5430'), ((166, 32, 262, 41), 'Panama, Rep. Panama'), ((166, 42, 248, 51), 'Tel (507) 204 - 9415'), ((340, 42, 384, 48), 'INVOICE DATE'), ((420, 42, 452, 48), 'ORDER No'), ((481, 42, 521, 48), 'ORDER DATE'), ((556, 42, 584, 48), 'PAGE No'), ((345, 55, 380, 62), '17/06/2019'), ((492, 54, 510, 61), '5430'), ((539, 56, 555, 63), 'page'), ((565, 55, 568, 61), '1'), ((580, 54, 589, 61), '/ 2'), ((340, 77, 385, 83), 'CREDIT TERMS'), ((420, 77, 451, 83), 'DUE DATE'), ((482, 77, 531, 84), 'TOTAL AMOUNT'), ((559, 77, 589, 83), 'CURENCY'), ((1, 86, 35, 94), 'BILL TO'), ((336, 90, 389, 97), 'Cash in Advance'), ((418, 90, 453, 97), '17/06/2019'), ((486, 89, 527, 96), '$ 766,036.00'), ((566, 89, 583, 96), 'USD'), ((6, 103, 31, 110), 'C00095'), ((6, 113, 67, 120), 'TIONALE PTE LTD'), ((204, 117, 242, 123), 'INCO TERMS'), ((302, 117, 327, 123), 'VESSEL'), ((382, 117, 428, 123), 'CONTAINER No'), ((464, 117, 496, 123), "SEAL No's"), ((546, 117, 574, 123), 'CARRIER'), ((6, 122, 70, 129), '11-02 High Street Plaza'), ((215, 130, 231, 137), 'EXW'), ((6, 135, 56, 141), 'Singapur 179433'), ((6, 140, 38, 146), 'SINGAPUR'), ((285, 146, 349, 152), 'PORT OF DISCHARGE'), ((215, 152, 238, 158), 'B/L No'), ((286, 153, 348, 159), 'IFINAL DESTINATION'), ((393, 152, 429, 158), 'LOAD DATE'), ((483, 153, 495, 159), 'ETD'), ((558, 153, 571, 159), 'ETA'), ((6, 168, 53, 175), 'GAURAV JAIN'), ((2, 181, 38, 189), 'SHIP TO'), ((6, 199, 80, 207), '11-02 High Street Plaza'), ((186, 196, 231, 204), 'REMARKS'), ((6, 208, 45, 215), '77 High Street'), ((186, 207, 261, 214), 'LV ORDER 065 6/17/19'), ((6, 220, 56, 226), 'Singapur 179433'), ((6, 226, 38, 232), 'SINGAPUR'), ((1, 278, 19, 285), 'ITEM'), ((71, 278, 119, 285), 'DESCRIPTION'), ((293, 278, 310, 285), 'UOM'), ((343, 278, 358, 285), 'QTY'), ((386, 278, 421, 285), 'COUNTRY'), ((436, 278, 472, 285), 'UNITS /UO'), ((502, 278, 524, 285), 'PRICE'), ((558, 278, 590, 285), 'AMOUNT'), ((1, 287, 27, 293), '03-00312'), ((70, 287, 188, 293), 'BURBERRY FOR WOMAN EDP SPR 50ML'), ((291, 287, 313, 294), 'UNITS'), ((345, 287, 358, 293), '467'), ((449, 287, 463, 294), 'TBD'), ((507, 287, 539, 294), '$ 16.0000'), ((575, 287, 605, 294), '7,472.00'), ((1, 302, 27, 308), '03-02470'), ((70, 302, 216, 308), 'BURBERRY LONDON FOR WOMEN EDP SPR 50ML'), ((291, 302, 312, 309), 'UNITS'), ((345, 302, 357, 308), '467'), ((449, 302, 463, 308), 'TBD'), ((507, 302, 539, 309), '$ 16.0000'), ((573, 302, 604, 309), '7,472.00'), ((1, 317, 27, 323), '03-07451'), ((71, 317, 174, 323), 'CHLOE SIGNATURE EDP SPR 50ML'), ((290, 317, 311, 324), 'UNITS'), ((345, 317, 357, 323), '120'), ((385, 317, 423, 324), 'Francia 422'), ((449, 317, 463, 324), 'TBD'), ((507, 317, 539, 324), '$ 32.0000'), ((571, 317, 605, 324), '$ 3,840.00'), ((1, 332, 27, 338), '03-07450'), ((71, 332, 174, 338), 'CHLOE SIGNATURE EDP SPR 75ML'), ((290, 332, 312, 339), 'UNITS'), ((346, 332, 358, 338), '120'), ((385, 332, 423, 339), 'Francia 422'), ((449, 332, 463, 338), 'TBD'), ((507, 332, 539, 339), '$ 41.0000'), ((571, 332, 605, 339), '$ 4,920.00'), ((1, 347, 27, 352), '03-08666'), ((71, 346, 176, 352), 'CK BEAUTY SHEER EDT SPR 100ML'), ((290, 347, 312, 354), 'UNITS'), ((345, 347, 357, 353), '600'), ((449, 347, 463, 354), 'TBD'), ((507, 347, 540, 354), '$ 14.0000'), ((571, 347, 605, 354), '$ 8,400.00'), ((1, 362, 27, 368), '03-10941'), ((70, 362, 163, 368), 'GUCCI BAMBOO EDP SPR 75ML'), ((289, 362, 313, 369), 'CASES'), ((345, 362, 358, 369), '360'), ((384, 362, 422, 369), 'Francia 422'), ((449, 362, 463, 369), 'TBD'), ((507, 362, 539, 369), '$ 40.0000'), ((567, 362, 604, 369), '$ 14,400.00'), ((1, 377, 28, 383), '03-25838'), ((71, 377, 148, 383), 'GUCCI BLOOM EDP 100ML'), ((291, 377, 312, 384), 'UNITS'), ((343, 377, 361, 384), '1,800'), ((449, 377, 464, 384), 'TBD'), ((507, 377, 539, 384), '$ 48.0000'), ((568, 377, 605, 384), '$ 86,400.00'), ((1, 392, 27, 398), '03-25836'), ((71, 392, 145, 398), 'GUCCI BLOOM EDP 50ML'), ((290, 392, 312, 399), 'UNITS'), ((343, 392, 361, 399), '1,000'), ((449, 392, 463, 399), 'TBD'), ((507, 392, 539, 399), '$ 37.0000'), ((567, 392, 604, 399), '$ 37,000.00'), ((1, 407, 27, 413), '03-09296'), ((71, 407, 180, 413), 'GUCCI GUILTY BLACK EDT SPR 75ML'), ((291, 407, 312, 414), 'UNITS'), ((343, 407, 361, 414), '1,500'), ((385, 407, 423, 414), 'Francia 422'), ((449, 407, 463, 413), 'TBD'), ((507, 407, 539, 414), '$ 35.5000'), ((567, 407, 604, 414), '$ 53,250.00'), ((1, 422, 28, 428), '03-36654'), ((70, 421, 165, 427), 'GUCCI GUILTY EDT SPR 2X5OML'), ((290, 422, 312, 429), 'UNITS'), ((345, 422, 357, 428), '584'), ((385, 422, 423, 429), 'Francia 422'), ((449, 422, 463, 429), 'TBD'), ((507, 422, 539, 429), '$ 54.0000'), ((568, 422, 605, 429), '$ 31,536.00'), ((1, 437, 28, 443), '03-07679'), ((70, 437, 158, 443), 'GUCCI GUILTY EDT SPR 75ML'), ((290, 437, 312, 444), 'UNITS'), ((342, 437, 360, 444), '1,200'), ((385, 437, 423, 444), 'Francia 422'), ((449, 437, 463, 444), 'TBD'), ((507, 437, 539, 444), '$ 35.5000'), ((567, 437, 604, 444), '$ 42,600.00'), ((1, 452, 27, 457), '03-09294'), ((70, 452, 189, 458), 'GUCCI GUILTY PH BLACK EDT SPR 90ML'), ((291, 452, 312, 459), 'UNITS'), ((343, 452, 361, 459), '1,500'), ((385, 452, 423, 459), 'Francia 422'), ((449, 452, 463, 459), 'TBD'), ((507, 452, 539, 459), '$ 34.0000'), ((568, 452, 605, 459), '$ 51,000.00'), ((1, 467, 27, 473), '03-08151'), ((71, 467, 202, 473), 'GUCCI GUILTY POUR HOMME EDT SPR 90ML'), ((291, 467, 312, 474), 'UNITS'), ((342, 467, 360, 474), '2,000'), ((385, 467, 423, 474), 'Francia 422'), ((449, 467, 464, 474), 'TBD'), ((507, 467, 539, 474), '$ 34.0000'), ((567, 467, 604, 474), '$ 68,000.00'), ((1, 482, 27, 488), '03-01288'), ((70, 482, 169, 488), 'HUGO DARK BLUE EDT SPR 75ML'), ((290, 482, 312, 489), 'UNITS'), ((342, 482, 361, 489), '2,000'), ((449, 482, 463, 488), 'TBD'), ((507, 482, 539, 489), '$ 14.0000'), ((567, 482, 604, 490), '$ 28,000.00'), ((1, 497, 27, 503), '03-01291'), ((71, 497, 167, 503), 'HUGO DEEP RED EDP SPR 90ML'), ((290, 497, 312, 504), 'UNITS'), ((342, 497, 360, 504), '2,400'), ((449, 497, 463, 504), 'TBD'), ((507, 497, 539, 504), '$ 18.0000'), ((568, 497, 605, 504), '$ 43,200.00'), ((1, 512, 28, 518), '03-10485'), ((70, 512, 205, 518), 'MARC JACOBS DAISY DREAM EDT SPR 100ML'), ((290, 512, 311, 519), 'UNITS'), ((345, 512, 358, 518), '480'), ((384, 512, 422, 519), 'Francia 422'), ((449, 512, 463, 519), 'TBD'), ((507, 512, 539, 519), '$ 33.0000'), ((567, 512, 604, 519), '$ 15,840.00'), ((1, 527, 27, 533), '03-08090'), ((71, 527, 228, 533), 'MARC JACOBS DAISY EAU SO FRESH EDT SPR 125ML'), ((291, 527, 312, 534), 'UNITS'), ((345, 528, 357, 534), '480'), ((449, 527, 463, 534), 'TBD'), ((507, 527, 539, 534), '$ 35.5000'), ((568, 527, 605, 534), '$ 17,040.00'), ((2, 542, 28, 548), '03-05139'), ((71, 542, 182, 548), 'MARC JACOBS DAISY EDT SPR 100ML'), ((291, 542, 312, 549), 'UNITS'), ((345, 543, 357, 549), '960'), ((449, 542, 463, 549), 'TBD'), ((507, 542, 539, 549), '$ 33.0000'), ((567, 542, 604, 549), '$ 31,680.00'), ((1, 557, 27, 563), '03-37536'), ((71, 557, 248, 563), 'MARC JACOBS DAISY MINI EDT SPR 4ML +DAISY FRESH 4ML'), ((291, 557, 312, 564), 'UNITS'), ((342, 558, 360, 565), '2,000'), ((449, 558, 463, 565), 'TBD'), ((507, 557, 539, 564), '$ 22.0000'), ((567, 557, 604, 564), '$ 44,000.00'), ((71, 563, 158, 569), '+DREAM 4ML +LOVE 4 4ML SET'), ((1, 572, 27, 578), '03-25853'), ((71, 572, 258, 578), 'MARC JACOBS DECADENCE EDP + DIVINE D EDT + DAISY EDT +'), ((290, 572, 312, 579), 'UNITS'), ((345, 572, 358, 579), '720'), ((385, 572, 423, 579), 'Francia 422'), ((449, 572, 463, 579), 'TBD'), ((507, 572, 539, 579), '$ 22.0000'), ((568, 572, 605, 579), '$ 15,840.00'), ((71, 578, 147, 584), 'DAISY DREAM EDT 4X4ML'), ((497, 609, 535, 616), 'SUBTOTAL'), ((563, 609, 604, 616), '$ 766,036.00'), ((504, 625, 535, 632), 'FREIGHT'), ((591, 625, 605, 632), '0.00'), ((493, 640, 535, 647), 'INSURANCE'), ((590, 640, 604, 647), '0.00'), ((26, 657, 60, 663), '# OF ITEMS'), ((99, 657, 152, 663), 'TOTAL QUANTITY'), ((211, 657, 225, 663), 'UOM'), ((272, 657, 329, 664), 'GROSS WEIGHT kg'), ((344, 657, 401, 664), 'GROSS CUBE, cbm'), ((473, 655, 534, 662), 'OTHER CHARGES'), ((591, 655, 605, 662), '0.00'), ((41, 668, 46, 674), '0'), ((115, 668, 138, 675), '20.758'), ((511, 684, 535, 691), 'TOTAL'), ((563, 685, 604, 692), '$ 766,036.00'), ((10, 701, 121, 709), 'SPECIAL INSTRUCTIONS:'), ((11, 747, 553, 754), 'Bank: Citibank N. A. 111 Wall St. New York City New York 10043. USA Swift CITUS33XXX Beneficiary Bank: BANISTMO, S. A. Torre Banistmo, Calle 50, Panama, A/C#: 36322415, Switf: MIDLPAPA'), ((11, 753, 404, 760), 'For Further Credit to Account #:36322415, Swift: MIDLPAPA. For Credit to Final Beneficiary: Licores de Venezuela, S.A.; Account #: 01-0017026-6'), ((340, 784, 382, 793), 'Approved:')], 2: [((166, 16, 299, 25), 'LICORES DE VENEZUELA, S.A.'), ((376, 14, 533, 25), 'PROFORMA INVOICE No.'), ((561, 15, 589, 26), '5430'), ((166, 32, 262, 41), 'Panama, Rep. Panama'), ((166, 42, 248, 51), 'Tel (507) 204 - 9415'), ((340, 42, 384, 48), 'INVOICE DATE'), ((420, 42, 452, 48), 'ORDER No'), ((481, 42, 521, 48), 'ORDER DATE'), ((556, 42, 583, 48), 'PAGE No'), ((345, 55, 380, 62), '17/06/2019'), ((492, 54, 510, 61), '5430'), ((539, 56, 555, 63), 'page'), ((564, 55, 569, 61), '2'), ((580, 55, 589, 62), '/ 2'), ((340, 77, 385, 83), 'CREDIT TERMS'), ((420, 77, 451, 83), 'DUE DATE'), ((482, 77, 531, 84), 'TOTAL AMOUNT'), ((559, 77, 590, 83), 'CURENCY'), ((1, 86, 36, 94), 'BILL TO'), ((336, 90, 389, 97), 'Cash in Advance'), ((418, 90, 453, 96), '17/06/2019'), ((486, 89, 527, 96), '$ 766,036.00'), ((566, 89, 583, 96), 'USD'), ((6, 103, 31, 110), 'C00095'), ((6, 113, 67, 120), 'TIONALE PTE LTD'), ((204, 117, 242, 124), 'INCO TERMS'), ((303, 117, 328, 123), 'VESSEL'), ((382, 117, 428, 123), 'CONTAINER No'), ((464, 117, 496, 123), "SEAL No's"), ((546, 117, 574, 123), 'CARRIER'), ((6, 122, 70, 129), '11-02 High Street Plaza'), ((215, 130, 231, 137), 'EXW'), ((6, 135, 56, 141), 'Singapur 179433'), ((6, 140, 38, 146), 'SINGAPUR'), ((285, 146, 349, 152), 'PORT OF DISCHARGE'), ((215, 152, 238, 158), 'B/L No'), ((286, 153, 349, 159), 'IFINAL DESTINATION'), ((393, 153, 429, 159), 'LOAD DATE'), ((483, 153, 495, 159), 'ETD'), ((558, 153, 571, 159), 'ETA'), ((6, 168, 53, 175), 'GAURAV JAIN'), ((2, 181, 38, 189), 'SHIP TO'), ((6, 199, 81, 207), '11-02 High Street Plaza'), ((186, 196, 231, 204), 'REMARKS'), ((6, 208, 45, 215), '77 High Street'), ((186, 207, 262, 214), 'LV ORDER 065 6/17/19'), ((6, 220, 56, 226), 'Singapur 179433'), ((6, 226, 38, 232), 'SINGAPUR'), ((2, 258, 20, 264), 'ITEM'), ((71, 258, 119, 265), 'DESCRIPTION'), ((293, 258, 310, 265), 'UOM'), ((344, 258, 359, 265), 'QTY'), ((386, 258, 421, 265), 'COUNTRY'), ((436, 258, 478, 265), 'UNITS IUOM'), ((502, 258, 525, 264), 'PRICE'), ((558, 258, 590, 264), 'AMOUNT'), ((1, 267, 28, 273), '03-11346'), ((70, 267, 142, 273), 'MIU MIU EDP SPR 100ML'), ((291, 267, 313, 274), 'UNITS'), ((345, 267, 358, 273), '480'), ((380, 267, 429, 274), 'Unidos de Ame'), ((449, 267, 463, 274), 'TBD'), ((507, 267, 539, 274), '$ 40.5000'), ((568, 267, 605, 274), '$ 19,440.00'), ((1, 282, 27, 288), '03-11345'), ((71, 282, 140, 288), 'MIU MIU EDP SPR 50ML'), ((291, 282, 312, 289), 'UNITS'), ((345, 282, 357, 288), '480'), ((380, 282, 429, 289), 'Unidos de Ame'), ((449, 282, 463, 289), 'TBD'), ((507, 282, 539, 289), '$ 30.5000'), ((567, 282, 604, 289), '$ 14,640.00'), ((1, 297, 27, 303), '03-25632'), ((71, 297, 183, 303), "MIU MIU L'EAU BLEUE EDP SPR 100ML"), ((291, 297, 312, 303), 'UNITS'), ((345, 297, 357, 303), '480'), ((380, 297, 429, 304), 'Unidos de Ame'), ((449, 297, 463, 304), 'TBD'), ((507, 297, 539, 304), '$ 40.5000'), ((568, 297, 605, 304), '$ 19,440.00'), ((1, 312, 27, 318), '03-25631'), ((71, 312, 179, 318), "MIU MIU L'EAU BLEUE EDP SPR 50ML"), ((291, 312, 312, 319), 'UNITS'), ((345, 312, 357, 318), '480'), ((380, 312, 429, 319), 'Unidos de Ame'), ((449, 312, 463, 319), 'TBD'), ((507, 312, 539, 319), '$ 32.0000'), ((568, 312, 605, 319), '$ 15,360.00'), ((1, 327, 28, 333), '03-36924'), ((71, 327, 184, 333), "MIU MIU L'EAU ROSEE EDT SPR 100ML"), ((290, 327, 311, 334), 'UNITS'), ((345, 327, 358, 333), '480'), ((449, 327, 463, 334), 'TBD'), ((507, 327, 539, 334), '$ 36.0000'), ((568, 327, 605, 334), '$ 17.280.00'), ((1, 342, 27, 348), '03-36923'), ((71, 342, 181, 348), "MIU MIU L'EAU ROSEE EDT SPR 50ML"), ((291, 342, 312, 349), 'UNITS'), ((345, 342, 358, 348), '480'), ((449, 342, 463, 349), 'TBD'), ((507, 342, 539, 349), '$ 29.0000'), ((567, 342, 604, 349), '$ 13,920.00'), ((1, 357, 27, 363), '03-36967'), ((71, 357, 192, 363), 'MIU MIU MULTILINE PRM SET 4EDP 18 TR'), ((290, 357, 312, 364), 'UNITS'), ((343, 357, 361, 364), '1,200'), ((449, 357, 464, 364), 'TBD'), ((507, 357, 539, 364), '$ 22.0000'), ((567, 357, 604, 364), '$ 26,400.00'), ((1, 372, 27, 378), '03-36614'), ((71, 372, 158, 378), 'TIFFANY & CO EDP SPR 50ML'), ((291, 372, 312, 379), 'UNITS'), ((345, 372, 357, 378), '360'), ((385, 372, 423, 379), 'Francia 422'), ((449, 372, 463, 379), 'TBD'), ((507, 372, 539, 379), '$ 38.0000'), ((568, 372, 605, 379), '$ 13,680.00'), ((1, 387, 27, 393), '03-07064'), ((71, 387, 145, 393), 'CK FREE EDT SPR 100ML'), ((290, 387, 311, 394), 'UNITS'), ((345, 387, 357, 393), '999'), ((449, 387, 464, 393), 'TBD'), ((507, 387, 539, 394), '$ 14.0000'), ((568, 387, 605, 394), '$ 13,986.00'), ((497, 609, 535, 616), 'SUBTOTAL'), ((563, 609, 604, 617), '$ 766,036.00'), ((504, 625, 535, 632), 'FREIGHT'), ((591, 625, 605, 632), '0.00'), ((493, 640, 535, 647), 'INSURANCE'), ((591, 640, 605, 647), '0.00'), ((26, 657, 60, 663), '# OF ITEMS'), ((99, 657, 152, 663), 'TOTAL QUANTITY'), ((211, 657, 225, 663), 'UOM'), ((272, 657, 329, 664), 'GROSS WEIGHT kg'), ((344, 657, 401, 664), 'GROSS CUBE, cbm'), ((473, 655, 535, 662), 'OTHER CHARGES'), ((591, 655, 605, 662), '0.00'), ((41, 668, 46, 674), '0'), ((115, 668, 137, 675), '26.197'), ((361, 668, 383, 675), '250.00'), ((511, 684, 535, 691), 'TOTAL'), ((563, 685, 604, 692), '$ 766,036.00'), ((10, 701, 121, 709), 'SPECIAL INSTRUCTIONS:'), ((11, 747, 552, 754), 'Bank: Citibank N. A. 111 Wall St. New York City, New York 10043. USA Swift CITUS33XXX Beneficiary Bank: BANISTMO, S. A. Torre Banistmo, Calle 50, Panama, A/C#: 36322415, Switf: MIDLPAPA'), ((11, 754, 404, 761), 'For Further Credit to Account #:36322415, Swift: MIDLPAPA. For Credit to Final Beneficiary: Licores de Venezuela, S.A. Account #: 01-0017026-6'), ((340, 784, 381, 794), 'Approved:')]}, 608, 796
        if words:
            for page_no, word_dict in words.items():
                lines_dict = convert_text_to_lines(words)
                print('lines_dict:------->', lines_dict)
                header_coords, line = get_table_labels(lines_dict)
                header_dict = adjust_table_header(line, header_coords, width)
                table_top = list(header_dict.values())[0][0][1]
                items_list = get_mapped_data(lines_dict, table_top,
                                             header_dict)
                print("items_list", items_list)
                # processed_at = datetime.now()
                # db, curr = connect_mysql()
                # curr.execute("INSERT INTO invoice_data_results (doc_data, processed_at)VALUES (%s, %s)",
                #              (thwart(json.dumps(items_list)),
                #               processed_at))
                # db.commit()
                # curr.close()
                # db.close()
                # return render_template('result.html',
                #                        items_list=items_list,
                #                        processed_at=processed_at)
                # invoice_details = get_other_invoice_details(lines_dict,
                #                                             table_top)
                # print("ID", invoice_details)
                # invoice_details.update({'items_list': items_list})
                # print(invoice_details)

    return render_template('upload_file.html')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5200)
