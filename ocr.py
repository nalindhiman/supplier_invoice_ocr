import boto3
import time
from config import AWS_REGION
from config import AWS_ACCESS_KEY_ID
from config import AWS_SECRET_ACCESS_KEY
from config import AWS_S3_BUCKET
from PyPDF2 import PdfFileReader


class AWSTextract(object):
    def __init__(self):
        pass

    def connect_textract(self):
        return boto3.client('textract',
                            region_name=AWS_REGION,
                            aws_access_key_id=AWS_ACCESS_KEY_ID,
                            aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

    def connect_S3(self):
        return boto3.client('s3',
                            region_name=AWS_REGION,
                            aws_access_key_id=AWS_ACCESS_KEY_ID,
                            aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

    def upload_to_s3(self, s3_client, img_path, img_name):
        while True:
            try:
                s3_client.upload_file(img_path, AWS_S3_BUCKET, img_name)
                break
            except Exception as e:
                print(e)

    def download_from_s3(self, s3_client, s3_path, local_path):
        try:
            s3_client.download_file(AWS_S3_BUCKET,
                                    s3_path,
                                    local_path)
        except Exception as e:
            print(e)

    def start_textract_job(self, textract_client, file_name):
        while True:
            try:
                response = textract_client.start_document_text_detection(
                    DocumentLocation={
                        'S3Object': {
                            'Bucket': AWS_S3_BUCKET,
                            'Name': file_name
                        }
                    }
                )
                if 'JobId' in response:
                    print(response['JobId'])
                    return response['JobId']
            except Exception as e:
                time.sleep(2)
                print(e)
        return False

    def get_textract_job_result(self, textract_client,
                                job_id, filepath, block_type='WORD'):
        while True:
            try:
                text = textract_client.get_document_text_detection(
                    JobId=job_id)
                if 'JobStatus' in text and text['JobStatus'] == "SUCCEEDED":
                    pdf = PdfFileReader(open(filepath, 'rb'))
                    if pdf.isEncrypted:
                        pdf.decrypt('')

                    width = int(pdf.getPage(0).mediaBox.getWidth())
                    height = int(pdf.getPage(0).mediaBox.getHeight())

                    page_no = 1
                    word_coordinates = {1: []}
                    for word in text['Blocks']:
                        if word['BlockType'] == block_type:
                            # check for new page
                            if word['Page'] != page_no:
                                page_no = word['Page']
                                word_coordinates[int(page_no)] = []

                            bbox = word['Geometry']['BoundingBox']
                            left = round(bbox['Left'] * width)
                            top = round(bbox['Top'] * height)
                            right = round(bbox['Width'] * width)
                            bottom = round(bbox['Height'] * height)
                            word = word['Text']
                            word_coordinates[page_no].append(((
                                left, top, right + left, bottom + top), word))
                    # print(word_coordinates, width, height)
                    return word_coordinates, width, height
                else:
                    time.sleep(2)
            except Exception as e:
                time.sleep(2)
                print(e)
        return False
